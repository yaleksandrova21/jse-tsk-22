package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.api.repository.IOwnerRepository;
import ru.yaleksandrova.tm.api.sevice.IOwnerService;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyIndexException;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.exception.system.IndexIncorrectException;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public E findByIndex(String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.size(userId)) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        repository.clear(userId);
    }

    public void remove(final String userId, final E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.remove(userId, entity);
    }

    @Override
    public Integer size(final String userId) {
        return repository.size(userId);
    }

}
