package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.repository.IAuthRepository;
import ru.yaleksandrova.tm.api.sevice.IAuthService;
import ru.yaleksandrova.tm.api.sevice.IUserService;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.AbstractException;
import ru.yaleksandrova.tm.exception.empty.EmptyIdException;
import ru.yaleksandrova.tm.exception.empty.EmptyLoginException;
import ru.yaleksandrova.tm.exception.empty.EmptyPasswordException;
import ru.yaleksandrova.tm.exception.entity.UserNotFoundException;
import ru.yaleksandrova.tm.exception.system.AccessDeniedException;
import ru.yaleksandrova.tm.model.User;
import ru.yaleksandrova.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IAuthRepository authRepository;

    private final IUserService userService;

    public AuthService(IAuthRepository authRepository, IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() {
        final String userId = authRepository.getCurrentUserId();
        if (userId == null) throw new EmptyIdException();
        return userId;
    }

    @Override
    public User getUser() {
        final String userId = getCurrentUserId();
        return userService.findById(userId);
    }

    @Override
    public void setCurrentUserId(String userId) {
        authRepository.setCurrentUserId(userId);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() {
        if (!isAuth()) throw new AccessDeniedException();
        System.out.println("Logout: " + userService.findById(getCurrentUserId()).getFirstName());
        setCurrentUserId(null);
    }

    @Override
    public boolean isAuth() {
        final String currentUserId = authRepository.getCurrentUserId();
        return !(currentUserId == null || currentUserId.isEmpty());
    }

    @Override
    public void checkRoles(final Role... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
