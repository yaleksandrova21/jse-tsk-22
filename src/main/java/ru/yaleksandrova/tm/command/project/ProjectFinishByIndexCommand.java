package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectFinishByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = serviceLocator.getProjectService().finishByIndex(userId, index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        serviceLocator.getProjectService().finishByIndex(userId, index);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
