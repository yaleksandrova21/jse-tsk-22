package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = serviceLocator.getProjectService().findByIndex(userId, index);
        if (project==null){
            throw new ProjectNotFoundException();
        }
        showProject(project);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
