package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change project status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
