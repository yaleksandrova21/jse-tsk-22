package ru.yaleksandrova.tm.command.project;

import ru.yaleksandrova.tm.command.AbstractProjectCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectTaskService().removeById(userId, id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        System.out.println("[Project deleted]");
        showProject(project);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
