package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.ProjectNotFoundException;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER TASK ID]");
        final String taskId = ApplicationUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = ApplicationUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        final Task taskUpdated = serviceLocator.getProjectTaskService().bindTaskById(userId, projectId, taskId);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
