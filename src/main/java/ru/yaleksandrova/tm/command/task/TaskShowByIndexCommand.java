package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER ID]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null){
            throw new TaskNotFoundException();
        }
        showTask(task);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
