package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.enumerated.Sort;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = ApplicationUtil.nextLine();
        final List<Task> tasks;
        if (sort == null || sort.isEmpty())
            tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            final Sort sortType = Sort.valueOf(sort);
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        for (Task task: tasks) showTask(task);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
