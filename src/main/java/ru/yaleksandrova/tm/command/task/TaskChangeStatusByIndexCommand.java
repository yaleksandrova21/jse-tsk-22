package ru.yaleksandrova.tm.command.task;

import ru.yaleksandrova.tm.command.AbstractTaskCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.exception.entity.TaskNotFoundException;
import ru.yaleksandrova.tm.model.Task;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-change-status-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change task status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeStatusByIndex(userId, index, status);
        if (task == null) {
            throw new TaskNotFoundException();
        }
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}
