package ru.yaleksandrova.tm.command.user;

import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserByLoginRemoveCommand extends AbstractCommand {
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = ApplicationUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
