package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;

import java.util.Date;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Project findByName(String userId, String name) {
        return list.stream()
                .filter(o -> userId.equals(o.getUserId()) && name.equals(o.getName()))
                .findFirst().orElse(null);
    }

    @Override
    public Project removeByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startByIndex(String userId, Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startById(String userId, String id) {
        final Project project = findById(userId, id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String userId, String id) {
        final Project project = findById(userId, id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(String userId, Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(String userId, String name) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusById(String userId, String id, Status status) {
        final Project project = findById(userId, id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(String userId, Integer index, Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String userId, String name, Status status) {
        final Project project = findByName(userId, name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
