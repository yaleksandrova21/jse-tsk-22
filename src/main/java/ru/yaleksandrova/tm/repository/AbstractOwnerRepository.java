package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IOwnerRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.clear();
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) {
        for(E entity:list){
            if(id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> ownerEntities =  Optional.ofNullable(findById(userId, id));
        ownerEntities.ifPresent(this::remove);
        return ownerEntities.orElse(null);
    }

    public Integer size(final String userId) {
        final List<E> list = findAll(userId);
        return list.size();
    }

}


