package ru.yaleksandrova.tm.repository;

import ru.yaleksandrova.tm.api.repository.IUserRepository;
import ru.yaleksandrova.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter(o -> login.equals(o.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(String email) {
        return list.stream()
                .filter(o -> email.equals(o.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

}
