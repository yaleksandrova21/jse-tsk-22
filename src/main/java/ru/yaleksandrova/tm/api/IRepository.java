package ru.yaleksandrova.tm.api;

import ru.yaleksandrova.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

     void add(final E entity);

     void remove(final E entity);

     List<E> findAll();

     List<E> findAll(Comparator<E> comparator);

     void clear();

     int size();

     boolean existsById(final String id);

     E findById(final String id);

     E findByIndex(final Integer index);

     E removeById(String id);

     E removeByIndex(String userId, Integer index);

}
