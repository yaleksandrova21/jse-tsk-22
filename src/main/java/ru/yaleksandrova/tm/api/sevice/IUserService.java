package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password, String email);

    User create(String login, String password, String email, Role role);

    User findById(String id);

    User findByLogin(String login);

    User updateUser(String id, String firstName, String lastName, String middleName, String email);

    User updateUserByLogin(String login, String firstName, String lastName, String middleName, String email);

    User setPassword(String id, String password);

    User setRole(String id, Role role);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

    User removeByLogin(String login);

}
