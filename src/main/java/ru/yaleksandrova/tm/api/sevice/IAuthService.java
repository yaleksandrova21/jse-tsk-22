package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.model.User;

public interface IAuthService {

    String getCurrentUserId();

    User getUser();

    void setCurrentUserId(String userId);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    void checkRoles(Role... roles);

}
