package ru.yaleksandrova.tm.api.sevice;

public interface IServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
