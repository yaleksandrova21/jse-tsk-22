package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import java.util.List;
import java.util.Comparator;

public interface IProjectService extends IOwnerService<Project> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Project findByName(String userId, String name);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project removeByName(String userId, String name);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project startById(String userId, String id);

    Project finishById(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusByName(String userId, String name, Status status);

}
