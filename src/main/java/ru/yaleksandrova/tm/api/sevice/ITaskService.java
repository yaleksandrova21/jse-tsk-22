package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;
import java.util.Comparator;

public interface ITaskService extends IOwnerService<Task> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Task findByName(String userId, String name);

    Task updateById(final String userId, final String id, final String name, final String description);

    Task updateByIndex(final String userId, final Integer index, final String name, final String description);

    Task removeByName(String userId, String name);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

}
