package ru.yaleksandrova.tm.api.entity;

public interface IWBS extends IHasCreated, IHasStartDate, IHasStatus, IHasName {
}
