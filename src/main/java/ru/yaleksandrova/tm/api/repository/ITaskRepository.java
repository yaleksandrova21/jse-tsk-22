package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;
import java.util.Comparator;

public interface ITaskRepository extends IOwnerRepository<Task> {

    Task findByName(String userId, String name);

    Task removeByName(String userId, String name);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task startById(String userId, String id);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task unbindTaskById(String userId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

}
